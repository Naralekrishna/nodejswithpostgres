const dbPool = require(`./../lib/pgpool`);
const bcrypt = require(`bcrypt`);

const webSites = async () => {
    try {
        const webSites = await dbPool.query(`select * from public.web_sites order by id desc`);
        if (webSites.rowCount > 0) { return { status: 200, message: `found successfully`, data: webSites.rows }; } else { return { status: 200, message: `no recods available`, data: [] } };
    } catch (error) {
        throw error
    }
}

const webSite = async ({ id }) => {
    try {
        const webSite = await dbPool.query(`select * from public.web_sites where id = $1`, [id]);
        if (webSite.rowCount > 0) { return { status: 200, message: `found successfully`, data: webSite.rows[0] }; } else { return { status: 200, message: `no recod available`, data: [] } };
    } catch (error) {
        throw error
    }
}

const create = async ({ web_site, password }) => {
    try {
        // Validations
        const webSiteExist = await dbPool.query(`select web_site from public.web_sites where lower(web_site) = lower($1)`, [web_site]);
        if (webSiteExist.rowCount > 0) { return { status: 400, message: `web site already exist` } };

        const encPassword = await bcrypt.hash(password, 10);
        const webSite = await dbPool.query(`INSERT INTO public.web_sites (web_site, "password", created_at, updated_at) VALUES($1, $2, now(), $3) RETURNING id, web_site`, [web_site, encPassword, null]);
        if (webSite.rowCount > 0) { return { status: 201, message: `created successfully`, data: webSite.rows }; }
    } catch (error) {
        throw error
    }
}

const multipleDelete = async ({ ids }) => {
    try {
        const result = await dbPool.query(`delete from public.web_sites where id = any($1)`, [ids]);
        return { status: 200, message: `deleted successfully` }
    } catch (error) {
        throw error;
    }
}

const update = async ({ id }, { web_site, password }) => {
    try {
        // Validation
        const webSiteExist = await dbPool.query(`select web_site from public.web_sites where lower(web_site) = lower($1) and id != $2`, [web_site, id]);
        if (webSiteExist.rowCount > 0) { return { status: 400, message: `web site already exist` } };

        const encPassword = await bcrypt.hash(password, 10);
        const webSite = await dbPool.query(`update public.web_sites set web_site = $1, password = $2, updated_at = now() where id = $3 RETURNING web_site`, [web_site, encPassword, id]);
        if (webSite.rowCount > 0) { return { status: 200, message: `updated successfully`, data: webSite.rows } };
    } catch (error) {
        throw error;
    }
}


module.exports = { webSites, webSite, create, multipleDelete, update }