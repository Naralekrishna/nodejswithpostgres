var express = require('express');
var router = express.Router();
const webSiteService = require(`../services/webSiteService`);

router.get('/', async (req, res, next) => {
  try {
    const webSites = await webSiteService.webSites();
    res.status(webSites.status).json(webSites);
  } catch (error) {
    res.status(500).json({ status: 500, message: error.message, data: [] });
  }
});

router.get('/:id', async (req, res, next) => {
  try {
    const webSite = await webSiteService.webSite(req.params);
    res.status(webSite.status).json(webSite);
  } catch (error) {
    res.status(500).json({ status: 500, message: error.message, data: [] });
  }
});

router.post('/', async (req, res, next) => {
  try {
    const webSite = await webSiteService.create(req.body);
    res.status(webSite.status).json(webSite);
  } catch (error) {
    res.status(500).json({ status: 500, message: error.message, data: [] });
  }
});

router.post('/delete', async (req, res, next) => {
  try {
    const result = await webSiteService.multipleDelete(req.body);
    res.status(result.status).json(result);
  } catch (error) {
    res.status(500).json({ status: 500, message: error.message, data: [] });
  }
});

router.put('/update/:id', async (req, res, next) => {
  try {
    const result = await webSiteService.update(req.params, req.body);
    res.status(result.status).json(result);
  } catch (error) {
    res.status(500).json({ status: 500, message: error.message, data: [] });
  }
});

module.exports = router;
